function GetFromApiTest(endpoint, parameters = "", body = "") {

    let myHeaders = new Headers();

    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };
    fetch(`http://127.0.0.1:5000/api/${endpoint}${parameters}`, requestOptions).then(res => res.json()).then(data => {
        console.log(data)
    });
}

async function PostFromApiTest(endpoint, parameters = "", body = "") {

    let myHeaders = new Headers();

    let requestOptions = {
        method: 'POST',
        headers: myHeaders,
        redirect: 'follow',
        body: body
    };
    return await fetch(`http://127.0.0.1:5000/api/${endpoint}${parameters}`, requestOptions).then(res => res.json()).then(data => {
        return data
    });
}

async function GetFromApi(endpoint, parameters = "") {

    let myHeaders = new Headers();

    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };
    return await fetch(`http://127.0.0.1:5000/api/${endpoint}${parameters}`, requestOptions).then(res => res.json()).then(data => {
        return data
    });
}

async function PostFromApi(endpoint, parameters = "", body = "") {

    let myHeaders = new Headers();

    let requestOptions = {
        method: 'POST',
        headers: myHeaders,
        redirect: 'follow',
        body: body
    };
    return await fetch(`http://127.0.0.1:5000/api/${endpoint}${parameters}`, requestOptions).then(res => res.json()).then(data => {
        return data
    });
}

async function GetXData(object) {
    let response;
    switch (object) {
        case "menu":
            response = await GetFromApi("admin/menu");
            return response["message"]
        case "logs":
            response = await GetFromApi("admin/logfiles");
            return response["message"]
        default:
            return {}
    }
}