async function loadHTMLPage(page) {
    if (history.pushState) {
        let newURL = window.location.protocol + "//" + window.location.host + window.location.pathname + `?page=${page}`;
        window.history.pushState({path: newURL}, '', newURL);
        fetch(`pages/${page}.html`).then(response => response.text()).then(text => document.getElementById('pages').innerHTML = text);
        document.getElementById('page_name').innerText = page.charAt(0).toUpperCase() + page.slice(1);
    }
}

function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');

    let regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'), results = regex.exec(url);

    if (!results) return "home";
    if (!results[2]) return "home";

    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

window.addEventListener('load', (event) => {
    loadHTMLPage(getParameterByName("page")).then(r => r)
});
