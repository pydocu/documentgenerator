import re
import os

from datetime import datetime


def save_json_file(form: object) -> str | None:
    if not re.match('^.*\\.json$', form.file.filename):
        return None

    if form.file_is_temporary:
        file_path = os.path.join("documentDB", "temporary", f"{datetime.now().strftime('%Y%m%d%H%M%S')}_temp.json")
        form.file.save(file_path)
    else:
        file_path = os.path.join("documentDB", "saved", form.file.filename)
        form.file.save(file_path)
    return file_path
