from pydantic import Field
from flask_openapi3 import HTTPBase


class HTTPBasic(HTTPBase):
    basicInfoFunc_ = Field(default="app.auth", alias="x-basicInfoFunc")
