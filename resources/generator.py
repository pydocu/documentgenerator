from datetime import datetime
from abc import ABC, abstractmethod


class Generator(ABC):

    @abstractmethod
    def setup_generator(self, cls: dict) -> (str, datetime):
        pass

    @abstractmethod
    def generator(self, filename: str, options: dict) -> object | None:
        pass
