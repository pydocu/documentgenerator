from pydantic import BaseModel, Field


class OkResponses(BaseModel):
    code: int = Field(200, description="status code")
    message: str = Field("Ok", description="exception information")


class BadRequestResponses(BaseModel):
    code: int = Field(400, description="status code")
    message: str = Field("Error", description="exception information")


class ErrorResponses(BaseModel):
    code: int = Field(404, description="status code")
    message: str = Field("Error", description="exception information")


class UnauthorizedResponses(BaseModel):
    code: int = Field(401, description="status code")
    message: str = Field("Error", description="Unauthorized user")


class ServerErrorResponses(BaseModel):
    code: int = Field(500, description="status code")
    message: str = Field("Error", description="exception information")
