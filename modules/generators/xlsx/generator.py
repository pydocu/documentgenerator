import io
import zipfile
import xlsxwriter

from os import remove
from pandas import DataFrame
from os.path import basename
from datetime import datetime
from xlsxwriter import Workbook
from resources.generator import Generator
from xlsxwriter.worksheet import Worksheet
from resources.decorators import singleton
from urllib.request import Request, urlopen
from resources.exception import GeneratorError
from xlsxwriter.utility import xl_cell_to_rowcol


@singleton
class XLXSGenerator(Generator):

    def setup_generator(self, generator_description: dict) -> tuple[str, datetime]:
        """
        Set up the Excel  generator and start process the JSON data
        :param generator_description: Full JSON structure
        :return: file name and generate time
        :rtype: tuple[str, datetime]
        """

        try:

            filename: str = generator_description['filename']
            xlsx_options: dict = generator_description['options']
            sheets: list[dict] = generator_description['sheets']
            raw_styles: dict = generator_description['styles']

            working_file = self.generator(f"documentDB/generated/xlsx/{filename}", xlsx_options)

            styles = self.__create_formats(working_file, raw_styles)

            for i in sheets:
                working_sheet = self.__add_sheet(working_file, i['sheetname'])
                self.__generate_process(working_sheet, styles, i['tables'])

            working_file.read_only_recommended()
            working_file.close()
            if 'use_zip64' in xlsx_options and xlsx_options['use_zip64']:
                with zipfile.ZipFile(
                        f"documentDB/generated/xlsx/zips/{basename(working_file.filename).replace('.xlsx', '.zip')}",
                        'w') as zip_file:
                    zip_file.write(working_file.filename, basename(working_file.filename))
                    remove(working_file.filename)

                return basename(working_file.filename).replace('.xlsx', '.zip'), datetime.now()

            return basename(working_file.filename), datetime.now()

        except Exception as e:
            raise GeneratorError(str(e))

    def generator(self, filename: str, options: dict) -> Workbook | None:
        """
        Create xlsx file foe working
        :param str filename: Xlsx file name
        :param dict options: Options for xlsx file (see: )
        :return: Xlsx file reference Or None value
        :rtype:  Workbook | None
        """

        if (filename.find('.') and filename.find('.xlsx')) > 0:
            return xlsxwriter.Workbook(filename, options)
        elif filename.find('.') == -1:
            return xlsxwriter.Workbook(f"{filename}.xlsx", options)
        else:
            return None

    def __add_sheet(workbook: Workbook, sheet_name: str) -> Worksheet:
        """
        Add sheet to the Excel
        :param workbook: Excel file
        :param sheet_name: Sheet name
        :return: new Sheet
        :rtype: Worksheet
        """

        return workbook.add_worksheet(sheet_name)

    def __create_formats(self, workbook: Workbook, raw_styles: dict) -> dict:
        """
        Create formatting library
        :param workbook: Excel file
        :param raw_styles: JSON input with raw stile description
        :return: Dictionary with style name and style object
        :rtype: dict
        """

        styles = dict()
        for style in raw_styles:
            try:
                styles[style] = workbook.add_format(raw_styles[style])
            except AttributeError as e:
                raise GeneratorError(str(e))
        return styles

    def __generate_process(self, worksheet: Worksheet, styles: dict, tables: list[dict]) -> None:
        """
        Process full of JSON data. Write and formatting the Excel file
        :param worksheet: Actual working sheet
        :param styles: All style in Excel file
        :param tables: Small part of Excel content
        :return: No return value
        :rtype: None
        """

        (last_row, last_col) = 0, 0
        title_start = None
        (content_start_row, content_start_col) = (None, None)

        for tables_value in tables:

            options = tables_value['options'] if 'options' in tables_value else None
            image = tables_value["image"] if 'image' in tables_value else None
            title = tables_value["title"] if 'title' in tables_value else None
            content = tables_value["content"] if 'content' in tables_value else None

            row, col = xl_cell_to_rowcol(options['startposition']) if 'startposition' in options else (last_row, last_col)

            orientation = options['orientation'] if 'orientation' in options else "horizontal"

            if image:
                worksheet = self.__insert_image(worksheet, image)

            if title:
                title_start = last_row, last_col, worksheet, (last_row, last_col) = self.__write_title(worksheet, title, styles, orientation, (row, col))
                row, col = (last_row, last_col)

            if content:
                content_start_row, content_start_col = last_row, last_col
                worksheet, (last_row, last_col) = self.__write_content(worksheet, content,
                                                                       styles, orientation, (row, col))
                row, col = (last_row, last_col)

            if title and 'merged' in title and title['merged']:
                worksheet.merge_range(title_start[0], title_start[1], content_start_row - 1, col, title['text'],
                                      styles[title['style']] if 'style' in title else None)

            last_orientation = options['orientationfromlast'] if 'orientationfromlast' in options else "horizontal"
            row, col = self.__push_table((content_start_row, content_start_col), (row, col), last_orientation)

    def __insert_image(self, worksheet: Worksheet, image: dict) -> Worksheet:
        """
        Inserting image in to the Excel sheet
        :param worksheet: Actual sheet
        :param image: Image JSON description (url, path, position)
        :return: Worksheet with inserted image
        :rtype: Worksheet
        """

        row, col = xl_cell_to_rowcol(image['position'])

        if 'url' in image and image['url']:
            req = Request(image['url'], headers={'User-Agent': 'Mozilla/5.0'})

            image_data = io.BytesIO(urlopen(req).read())
            worksheet.insert_image(row, col, image['url'], {'image_data': image_data})

        elif 'path' in image and image['path']:
            worksheet.insert_image(row, col, image['path'])

        return worksheet

    def __write_title(self, worksheet: Worksheet, title: dict, styles: dict,
                      orientation: str, positions: tuple[int, int]) -> tuple[Worksheet, tuple[int, int]]:
        """
        Write a title for a mini table in the Excel file
        :param worksheet: Working sheet
        :param title: Title description
        :param styles: Styles for formatting
        :param orientation: How to write the title is multi values. Horizontal or vertical
        :param positions: Starting position of table
        :return: Working sheet and last row and column
        :rtype: tuple[Worksheet, tuple[int, int]]
        """

        (start_row, start_col) = (row, col) = positions

        if type(title['text']) is str:
            worksheet.write(row, col, title['text'], styles[title['style']] if 'style' in title else None)
            (row, col) = self.__next_step((row, col), orientation, title["height"] if 'height' in title else 1)

        elif type(title['text']) is list:
            for element in title['text']:
                worksheet.write(row, col, element, styles[title['style']] if 'style' in title else None)
                (row, col) = self.__next_step((row, col), orientation, title["height"] if 'height' in title else 1)
            if orientation == "vertical":
                (row, col) = (row + 1, start_col)
        return worksheet, (row, col)

    def __write_content(self, worksheet: Worksheet, content: dict, styles: dict,
                        orientation: str, positions: tuple[int, int]) -> tuple[Worksheet, tuple[int, int]]:
        """
        Preparing for processing the content data
        :param worksheet: Working sheet
        :param content: Content description
        :param styles: Styles for formatting
        :param orientation: How to write the content of the table. Horizontal or vertical
        :param positions: Starting position of table
        :return: Working sheet and last row and column
        :rtype tuple[Worksheet, tuple[int, int]]
        """

        data: dict | list = content['data']
        style = content['contentstyle'] if 'contentstyle' in content else None
        col_style = content['columntitlestyle'] if 'columntitlestyle' in content else None

        if type(data) is dict:
            return self.__content_writer(worksheet, data, styles, orientation, positions, col_style, style)

        elif type(data) is list:
            data = DataFrame(data).to_dict(orient='list')
            return self.__content_writer(worksheet, data, styles, orientation, positions, col_style, style)

    def __content_writer(self, worksheet: Worksheet, data: dict, styles: dict,
                         orientation: str, positions: tuple[int, int],
                         col_style: str, style: str) -> tuple[Worksheet, tuple[int, int]]:
        """
        Write content to the Excel file
        :param worksheet: Working sheet
        :param data: Data for write.
        :param styles: Styles
        :param orientation: Horizontal or vertical table
        :param positions: Starting pos
        :param col_style: Column header style
        :param style: Content style
        :return: Working sheet and last row and column
        :rtype tuple[Worksheet, tuple[int, int]]
        """

        (row, col) = positions
        (star_row, start_col) = positions
        (last_row, last_col) = positions

        for content_key, content_value in data.items():
            worksheet.write(row, col, content_key, styles[col_style] if col_style else None)
            (row, col) = self.__next_step((row, col), orientation)

            for element in content_value:
                worksheet.write(row, col, element, styles[style] if style else None)
                (row, col) = self.__next_step((row, col), orientation)
                (last_row, last_col) = (row, col)
            (row, col) = self.__next_row((row, col), (star_row, start_col), orientation)

        return worksheet, (last_row, last_col)

    def __next_step(self, actual: tuple, orientation: str, step: int = 1) -> tuple[int, int]:
        """
        Calculate next row and column
        :param actual: Actual position
        :param orientation: Orientation
        :param step: How row or column
        :return: row, col
        :rtype tuple[int, int]
        """

        (row, col) = actual

        if orientation == "horizontal":
            return row + step, col
        else:
            return row, col + step

    def __next_row(self, actual: tuple[int, int], starts: tuple[int, int],
                   orientation: str, step: int = 1) -> tuple[int, int]:
        """
        Calculate next data row position
        :param actual: Actual position
        :param starts: Table starting position
        :param orientation: Orientation
        :param step: How row or column
        :return: row, col
        :rtype: tuple[int, int]
        """

        (row, col) = actual
        (start_row, start_col) = starts

        if orientation == "horizontal":
            return start_row, col + step
        else:
            return row + step, start_col

    def __push_table(self, last: tuple[int, int], actual: tuple[int, int],
                     direction: str, step: int = 2) -> tuple[int, int]:
        """
        Calculate next mini table position
        :param last: Last row and column the table
        :param actual: Actual row and column
        :param direction: Push direction
        :param step: How many cell pushing
        :return: Row and column
        :rtype: tuple[int, int]
        """

        row, col = actual
        last_row, last_col = last
        if direction == "horizontal":
            return last_row, col + step
        else:
            return row + step, last_col
