import json

from . import generator
from . import responses
from __main__ import APP
from .models import XlsxQuery
from .models import UploadFileForm

from flask import send_file
from http import HTTPStatus
from flask_openapi3 import Tag
from flask_openapi3 import APIBlueprint
from resources.utils import save_json_file
from resources.exception import GeneratorError

TAG: Tag = Tag(name='XLSX', description='XLSX file generator')

XLSX_GENERATOR = generator.XLXSGenerator()

API = APIBlueprint(
    "/xlsx",
    __name__,
    url_prefix='/api/xlsx',
    abp_tags=[TAG],
    abp_responses={
        "200": responses.Ok,
        "500": responses.ServerError,
        "401": responses.UnauthorizedResponses
    },
    doc_ui=True
)


@API.post('/getenratefromrequest')
def generate_from_request(body: XlsxQuery):
    """generate xlsx

    Require a filled, valid JSON file in a post request body. After the request generator process start.
    """

    try:
        (filename, timestamp) = XLSX_GENERATOR.setup_generator(body.xlsx_description)
    except GeneratorError as e:
        return responses.ServerError(message=str(e)).json()

    if not body.IsDownloadable:
        return responses.Ok(code=200, message={"tag": "File generated",
                                               "filename": filename, "timestamp": timestamp}).json(), HTTPStatus.OK
    else:
        if 'zip' in filename:
            FILEPATH = rf"documentDB/generated/xlsx/zip/{filename}"
        else:
            FILEPATH = rf"documentDB/generated/xlsx/{filename}"

        return send_file(FILEPATH, as_attachment=True)


@API.post('/generatefromfile', )
def generate_from_file(form: UploadFileForm):
    """upload generator file
        Require a filled, valid JSON file in a posted form. After the file is uploaded the generator process start.

        You can save the file later if needed.

        The saved file name is generate automatic or use the uploaded file name.

        Temporary file is deleted after the process.

        By default uploaded file is temporary.
    """

    file_path = save_json_file(form)

    if not file_path:
        return responses.ServerError(message="Not a JSON file").json(), HTTPStatus.INTERNAL_SERVER_ERROR

    with open(file_path, "r", encoding="utf8") as json_file:
        body = json.loads(json_file.read())

        try:
            filename, timestamp = XLSX_GENERATOR.setup_generator(body["xlsx_description"])
        except GeneratorError as e:
            return responses.ServerError(message=str(e)).json()

        return responses.Ok(code=200, message={"tag": "File generated",
                                               "filename": filename,
                                               "timestamp": timestamp}).json(), HTTPStatus.OK


APP.register_api(API)
