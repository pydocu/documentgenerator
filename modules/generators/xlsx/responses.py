from pydantic import Field
from datetime import datetime

from resources.responses import OkResponses as baseOk
from resources.responses import ErrorResponses as baseError
from resources.responses import BadRequestResponses as baseBad
from resources.responses import ServerErrorResponses as baseServerError
from resources.responses import UnauthorizedResponses as baseUnauthorizedResponses


class Ok(baseOk):
    code: int = Field(200, description="status code")
    message: dict = Field({"tag": "File generated",
                           "filename": f"generated_{datetime.now()}.xlsx",
                           "timestamp": datetime.now()},
                          description="exception information")


class Error(baseError):
    code: int = Field(404, description="status code")
    message: str = Field("Error in generated method", description="exception information")


class BadRequest(baseBad):
    code: int = Field(400, description="status code")
    message: str = Field("Bad API request", description="exception information")


class UnauthorizedResponses(baseUnauthorizedResponses):
    code: int = Field(401, description="status code")
    message: str = Field("Error", description="Unauthorized user")


class ServerError(baseServerError):
    code: int = Field(500, description="status code")
    message: str = Field(..., description="exception information")
