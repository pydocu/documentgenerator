from .PDFFactroy import get_pdf

from fpdf import FPDF
from os.path import basename
from datetime import datetime
from resources.generator import Generator
from resources.decorators import singleton
from resources.exception import GeneratorError


@singleton
class PDFGenerator(Generator):

    def setup_generator(self, generator_description: dict) -> tuple[str, datetime]:
        try:
            print(generator_description)
            pdf_type: str = generator_description["type"]
            filename: str = generator_description['filename']
            options: dict = generator_description['options']
            text: dict = generator_description['text']

            pdf = self.generator(filename, options, text=text, type=pdf_type)
            return basename(filename), datetime.now()
        except Exception as e:
            raise GeneratorError(str(e))

    def generator(self, filename: str, options: dict, **kwargs) -> FPDF | None:

        pdf = get_pdf(kwargs["type"], kwargs["text"], options)

        if (filename.find('.') and filename.find('.pdf')) > 0:
            return pdf.output(rf"documentDB/generated/pdf/{filename}")
        elif filename.find('.') == -1:
            return pdf.output(rf"documentDB/generated/pdf/{filename}.pdf")
        else:
            return None
