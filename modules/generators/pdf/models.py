from pydantic import BaseModel, Field
from flask_openapi3 import FileStorage


class PDFQuery(BaseModel):
    IsDownloadable: bool = Field(False, description="Download generated file overt HTTP")
    pdf_description: dict = Field(description="Json description for PDF file")


class UploadFileForm(BaseModel):
    file: FileStorage = Field(description="Json file")
    file_is_temporary: int = Field(1, description="Is a temporary file")
    IsDownloadable: bool = Field(False, description="Download generated file overt HTTP")
