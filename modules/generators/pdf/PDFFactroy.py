from fpdf import FPDF
from .instances import HtmlPDF


def get_pdf(pdf_type: str, text: str, options: dict) -> FPDF | None:
    match pdf_type:
        case 'base':
            pdf = FPDF()
            pdf.write(txt=text)
            return pdf
        case 'html':
            pdf = HtmlPDF()
            pdf.add_page()
            pdf.write_html(text)
            return pdf
        case _:
            return None
