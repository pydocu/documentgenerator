from pydantic import BaseModel, Field


class AuthQuery(BaseModel):
    username: str
    password: str
