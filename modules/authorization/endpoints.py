from __main__ import APP
from .models import AuthQuery

from flask_openapi3 import APIBlueprint

API = APIBlueprint(
    "/sec",
    __name__,
    doc_ui=True
)


@API.post('/auth')
def auth(header: AuthQuery):
    return {'error': 401}


APP.register_api(API)
