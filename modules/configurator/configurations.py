import json


class BaseConfiguration:

    def __init__(self, configuration_type: str, sql_connection=False):
        self.__configuration = self.__read_config_json(configuration_type)

    def get_property(self, property_name):
        if property_name not in self.__configuration.keys():
            return None
        return self.__configuration[property_name]

    def set_property(self, property_name, value):
        if property_name not in self.__configuration.keys():
            return None
        self.__configuration[property_name] = value

    @staticmethod
    def __read_config_json(configuration_type: str) -> dict:
        """
        Read a specific JSON file on the hard drive
        :return: JSON file content parsed to dict
        """

        with open("./envirements/config.json") as json_file:
            return json.loads(json_file.read())[configuration_type]


class DBConfiguration(BaseConfiguration):
    """DataBase configuration Object"""

    def __init__(self, configuration_type: str):
        super().__init__(configuration_type)

    @property
    def host(self) -> str | None:
        return self.get_property("host")

    @property
    def user(self) -> str | None:
        return self.get_property("user")

    @property
    def password(self) -> str | None:
        return self.get_property("password")

    @property
    def database(self) -> str | None:
        return self.get_property("database")


class APPConfiguration(BaseConfiguration):
    """Web APP Configuration Object"""

    def __init__(self, configuration_type: str):
        super().__init__(configuration_type)

    @property
    def admin_url(self) -> str:
        return self.get_property("admin_url")

    @property
    def redirect(self) -> bool:
        return self.get_property("redirect")
