from resources.decorators import singleton
from .configurations import *


@singleton
class Configurator:

    def __init__(self):
        self.__APP_CONFIGS: dict = {}
        self.__DB_CONNECTORS: dict = {}

    def register_new_db_connector(self, db_connector_name: str, db_engine) -> bool:
        if db_connector_name not in self.__DB_CONNECTORS:
            self.__DB_CONNECTORS[db_connector_name] = db_engine
            return True
        return False

    def get_db_connector(self, db_connector_name: str):
        if db_connector_name in self.__DB_CONNECTORS:
            return self.__DB_CONNECTORS[db_connector_name]
        return None

    def register_new_app_config(self, app_config_name: str, app_config) -> bool:
        if app_config_name not in self.__APP_CONFIGS:
            self.__APP_CONFIGS[app_config_name] = app_config
            return True
        return False

    def get_app_config(self, app_config_name: str):
        if app_config_name in self.__APP_CONFIGS:
            return self.__APP_CONFIGS[app_config_name]
        return None

    def init_start_configs(self):
        self.register_new_app_config("APPCONFIGS", APPConfiguration("app"))
        self.register_new_app_config("DBCONFIG", DBConfiguration("data_base_connection"))
