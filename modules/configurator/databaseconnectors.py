import sqlalchemy


class DatabaseConnection:

    def __init__(self, dialect: str, dbapi: str, user: str, password: str, host: str, database: str,
                 parameters: dict):
        self.__dialect: str = dialect
        self.__dbapi: str = dbapi
        self.__user: str = user
        self.__password: str = password
        self.__host: str = host
        self.__database: str = database
        self.__parameters: dict = parameters

    def get_engine(self) -> sqlalchemy.Engine:
        return sqlalchemy.create_engine(f"{self.__dialect}{'+' + self.__dbapi if self.__dbapi else '' }://"
                                        f"{self.__user}:{self.__password}@{self.__host}/{self.__database}",
                                        **self.__parameters)


class MariaDBConnection(DatabaseConnection):
    def __init__(self, database_engine: str, dbapi: str, user: str, password: str, host: str, database: str,
                 parameters: dict):
        super().__init__(database_engine, dbapi, user, password, host, database, parameters)

    def get_engine(self) -> sqlalchemy.Engine:
        return super().get_engine()
