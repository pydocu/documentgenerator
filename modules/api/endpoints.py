from modules.adminpage import endpoints
from modules.authorization import endpoints
from modules.generators.pdf import endpoints
from modules.generators.xlsx import endpoints
