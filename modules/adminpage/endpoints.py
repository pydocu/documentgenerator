from . import responses
from __main__ import APP

from os import listdir
from http import HTTPStatus
from flask_openapi3 import Tag
from os.path import isfile, join
from flask_openapi3 import APIBlueprint

TAG: Tag = Tag(name='Admin')

API = APIBlueprint(
    "/admin",
    __name__,
    url_prefix='/api/admin',
    abp_tags=[TAG],
    abp_responses={
        "200": responses.Ok,
        "500": responses.ServerError
    },
    doc_ui=True
)


@API.get('/health')
def health():
    return responses.Ok(message={"msg": "ok"}).json(), HTTPStatus.OK


@API.get('/menu')
def menus():
    return responses.Ok(code=200, message={"menus":
        [
            {"index": 1, "name": "Dashboard", "class": "text-2xl uil uil-estate", "redirect": "loadHTMLPage('home');"},
            {"index": 2, "name": "Settings", "class": "text-2xl uil uil-setting", "redirect": ""},
            {"index": 3, "name": "Files", "class": "text-2xl uil uil-file-search-alt", "redirect": ""},
            {"index": 4, "name": "Logs", "class": "text-2xl uil uil-server", "redirect": "loadHTMLPage('logs');"},
            {"index": 5, "name": "API Docs", "class": "text-2xl uil uil-file", "redirect": "location.href='/openapi';"},
        ]
    }).json(), HTTPStatus.OK


@API.get('/logfiles')
def logfiles():
    return responses.Ok(code=200, message={"files":
                                               [{"file": f, "content": getfileconetent(f)} for f in listdir("logs/") if
                                                isfile(join("logs/", f))]
                                           }).json(), HTTPStatus.OK


def getfileconetent(filename: str):
    with open(f"logs/{filename}", "r") as file:
        return file.read()


APP.register_api(API)
