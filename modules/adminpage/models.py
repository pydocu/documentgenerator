from pydantic import BaseModel, Field
from flask_openapi3 import FileStorage


class FileQuery(BaseModel):
    filename: str
