import logging

from flask import redirect
from flask_cors import CORS
from datetime import datetime
from flask_openapi3 import Info
from flask_openapi3 import OpenAPI
from flask_httpauth import HTTPBasicAuth

from modules.configurator.configurator import Configurator

CONFIGURATOR: Configurator = Configurator()
CONFIGURATOR.init_start_configs()

INFO = Info(title='PyDocument Generator', version='0.0.1')
APP = OpenAPI(__name__, info=INFO, static_url_path='', static_folder='client/')
AUTH = HTTPBasicAuth()


CORS(APP)


@APP.route(f"/{CONFIGURATOR.get_app_config('APPCONFIGS').admin_url}")
def index():
    return APP.send_static_file("index.html")


@APP.route("/")
def root():
    if CONFIGURATOR.get_app_config('APPCONFIGS').redirect:
        return redirect("/admin", code=302)
    return ""


# Import API endpoints NEED TO HERE
from modules.api import endpoints


if __name__ == '__main__':
    # logging.basicConfig(filename=f"logs/{datetime.date(datetime.now())}-logs.log")
    APP.run(debug=True)
